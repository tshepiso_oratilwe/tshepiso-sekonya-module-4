import 'package:flutter/material.dart';
import 'package:splash/profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.grey[100],
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.teal,
        ).copyWith(
          secondary: const Color.fromARGB(255, 150, 127, 0),
        ),
        textTheme: const TextTheme(
          bodyText2: TextStyle(
            color: Colors.blueGrey,
          ),
        ),
      ),
      home: const Profile(),
    );
  }
}
